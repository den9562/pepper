import argparse
import sys
import math
import tempfile
import subprocess
import itertools
import json
import re

import preflibtools.generate_profiles as gp

def output_election(prefs, pref_counts, ncand, nucand, nvoter, ofile, control):
    prefcounter = 0

    if nucand > 0:
        ofile.write("rcandnum(%d).\n" % ncand)
        ofile.write("ucandnum(%d).\n" % nucand)
    else:
        ofile.write("candnum(%d).\n" % ncand)
    
    ofile.write("voternum(%d).\n" % nvoter)
    ofile.write("prefnum(%d).\n" % len(prefs))

    #ofile.write('limit(' + str(limit) + ').\n\n')
    #ofile.write('preferredCand(' + str(pref) + ').\n\n')
    ofile.write("\n% ELECTION PREFERENCES AND PREFERENCE COUNTS\n")
    # print every preference
    for p in range(len(prefs)):
        #print(prefs[p])
        for rank in range(1, ncand+nucand+1):
            #print("p(%d, %d, %d). " % (p+1, rank, prefs[p][rank]))
            ofile.write("p(%d, %d, %d). " % (p+1, rank, prefs[p][rank]))
        ofile.write("\n")
        ofile.write("votecount(%d, %d).\n\n" % (p+1, pref_counts[p]))
        ofile.write("\n")

def run_score(cmap, rmaps, rmapscounts, output, clingo, score):
    candidates = [c for c in cmap]
    minscores = {}
    numcand = len(cmap)
    numvotes = sum(rmapscounts)
    minscore = numvotes*(numcand * (numcand -1) / 2)
    #votes is a reverse of rmaps
    votes = {}
    for p in range(len(rmaps)):
        votes[p] = {}
        for rank in rmaps[p]:
            votes[p][rmaps[p][rank]] = rank

    for c in cmap:
        minscores[c] = minscore
    for p in itertools.permutations(candidates):
        ktd = 0

        for i in range(0, len(p)):
            for j in range(i + 1, len(p)):
                for k in range(0, len(rmaps)):
                    if (votes[k][p[i]] > votes[k][p[j]]):
                        ktd += rmapscounts[k]

        if ktd < minscores[p[0]]:
            minscores[p[0]] = ktd
    for c in cmap:
        if minscores[c] < minscore:
            minscore = minscores[c]

        print("Kemeny score for %s is %d" % (cmap[c], minscores[c]))

    winners = [c for c in cmap if minscores[c] == minscore]

    if (clingo is not None) and (score is not None):
        clingout = subprocess.run([clingo, "--outf=2", score, output], stdout=subprocess.PIPE)
        clingoparsed = json.loads(clingout.stdout.decode('utf-8'))

        assert(clingoparsed["Result"] == "OPTIMUM FOUND")
        assert(clingoparsed["Models"]["Optimum"] == "yes")

        clingowinner = clingoparsed["Call"][0]["Witnesses"][-1]["Value"][0]
        clingoscore = int(clingoparsed["Call"][0]["Witnesses"][-1]["Costs"][0])
        clingomatch = re.match("winner\(([\d]+)\)", clingowinner)

        assert(clingomatch)
        assert(int(clingomatch.group(1)) in winners)
        assert(minscore == clingoscore)

    return winners

def run_control(cmap, rmaps, rmapscounts, numcand, numunregistered, k, output, clingo, control):
    basecandidates = list(cmap.keys())[:numcand]
    extracandidates = [c for c in cmap if c not in basecandidates]

    for c in basecandidates:
        ctrl = k + 1

        print("Calculating Kemeny control for %s" % cmap[c])

        for d in range(0, k + 1):
            for acomb in itertools.combinations(extracandidates, d):
                newcmap = {}

                for b in basecandidates:
                    newcmap[b] = cmap[b]

                for a in acomb:
                    newcmap[a] = cmap[a]

                if c in run_score(newcmap, rmaps, rmapscounts, output, None, None):
                    ctrl = d
                    break
                
            if ctrl < k + 1:
                break

        if ctrl < k + 1:
            print("\tFound control for %d added candidates" % d)

            if (clingo is not None) and (control is not None):
                yesrun = subprocess.run([clingo, "-q", control, output, "-"], input=b"limit(%d). preferredCand(%d).\n" % (k, c))

                assert(yesrun.returncode in [10, 30])

                if d > 0:
                    norun = subprocess.run([clingo, "-q", control, output, "-"], input=b"limit(%d). preferredCand(%d).\n" % (d - 1, c))

                    assert(norun.returncode == 20)
        else:
            print("\tThis candidate is out of control.")

            if (clingo is not None) and (control is not None):
                norun = subprocess.run([clingo, "-q", control, output, "-"], input=b"limit(%d). preferredCand(%d).\n" % (k, c))

                assert(norun.returncode == 20)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate random instances of Young score and control problems")

    parser.add_argument("C", help="Number of candidates", type=int)
    parser.add_argument("V", help="Number of voters", type=int)
    parser.add_argument("output", help="Output the election to this file", type=argparse.FileType('w'))
    parser.add_argument("--unregistered", "-u", help="Number of unregistered candidates for the control problem", type=int, default=0)
    parser.add_argument("--limit", "-k", help="Voter deletion limit for the control problem", type=int, default=0)
    parser.add_argument("--clingo", help="Clingo executable to run tests")
    parser.add_argument("--score", help="ASP encoding of the score problem to run tests")
    parser.add_argument("--control", help="ASP encoding of the control problem to run tests")

    res = parser.parse_args()

    cmap = gp.gen_cand_map(res.C + res.unregistered)
    rmaps, rmapscounts = gp.gen_urn_strict(res.V, math.factorial(res.C), cmap)
    #rmaps, rmapscounts = gp.gen_impartial_culture_strict(res.V, cmap)

    output_election(rmaps, rmapscounts, res.C, res.unregistered, res.V, res.output, None)

    res.output.flush()

    run_score(cmap, rmaps, rmapscounts, res.output.name, res.clingo, res.score)

    if res.limit > 0:
        run_control(cmap, rmaps, rmapscounts, res.C, res.unregistered, res.limit, res.output.name, res.clingo, res.control)
