import igraph
import sys
import random
import itertools
import argparse
import math

def fromg6(l):
    n = ord(l[0]) - 63
    k = int(math.ceil(n * (n - 1) / float(12)))
    assert(l[k + 1] == '\n')
    i = 0
    j = 1
    g = igraph.Graph(n)
    
    for b in range(1, k + 1):
        bstr = format(ord(l[b]) - 63, '06b')
        
        for bit in bstr:
            if bit == '1':
                g.add_edge(i, j)
                
            i += 1
            
            if i == j:
                i = 0
                j += 1

    return g

def print_basic_instance(g, lpfile, k):
    print("vertex(0..%d)." % (g.vcount() - 1), file=lpfile)

    for e in g.es:
        print("edge(%d,%d)." % (e.source, e.target), file=lpfile)

    for d in deletable:
        print("deletable(%d)." % d, file=lpfile)
        
    print("limit(%d)." % k, file=lpfile)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate random instances of vertex-member-select*")
    
    parser.add_argument("--probability", "-p", help="Erdos-Renyi probability", type=float, default=0.25)
    parser.add_argument("--read", "-r", help="Read graph from stdin in g6 format", default=False, action="store_true")
    parser.add_argument("n", help="Number of vertices in the graph", type=int)
    parser.add_argument("d", help="Number of deletable vertices", type=int)
    parser.add_argument("k", help="Delete limit", type=int)
    
    res = parser.parse_args()
    deletable = random.sample([i for i in range(0, res.n)], res.d)
    graphs = []
    
    if res.read:
        for l in sys.stdin:
            graphs.append(fromg6(l))
    else:
        graphs.append(igraph.Graph.Erdos_Renyi(res.n, res.probability))

    yes = 0
    no = 0

    for g in graphs:
        vhats = set()

        for v in g.vs:
            v["origidx"] = v.index

        for l in range(0, res.k + 1):
            for deleted in itertools.combinations(deletable, l):
                gdel = g.copy()
                mincovers = []

                gdel.delete_vertices(list(deleted))

                indsets = gdel.largest_independent_vertex_sets()

                for i in indsets:
                    mincover = set(range(0, gdel.vcount())) - set(i)
                    mincovers.append(mincover)

                for c in mincovers:
                    for v in c:
                        vhats.add(gdel.vs[v]["origidx"])
                        
        print("yes intances: %d" % len(vhats), file=sys.stderr)
        
        for v in vhats:
            with open("yes-%04d.lp" % yes, "w") as yesinstance:
                print_basic_instance(g, yesinstance, res.k)
                print("vhat(%d)." % v, file=yesinstance)
            
            yes += 1
            
        nos = set([v["origidx"] for v in g.vs]) - vhats
        
        for v in nos:
            with open("no-%04d.lp" % no, "w") as noinstance:
                print_basic_instance(g, noinstance, res.k)
                print("vhat(%d)." % v, file=noinstance)
                
            no += 1
