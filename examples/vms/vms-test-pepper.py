import sys
import glob
import os
import subprocess

if __name__ == "__main__":
    pepper = sys.argv[1]
    pimeta = sys.argv[2]
    clingo = sys.argv[3]
    vmsdir = sys.argv[4]
    yesinstances = glob.glob(os.path.join(vmsdir, "yes*.lp"))
    noinstances = glob.glob(os.path.join(vmsdir, "no*.lp"))
    
    for i in yesinstances:
        with subprocess.Popen([pepper, i, os.path.join(vmsdir, "guess.lp"), os.path.join(vmsdir, "vertexcover.lp")], stdout=subprocess.PIPE) as pepper_process:
            clingo_process = subprocess.run([clingo, "-q", pimeta, "-"], stdin=pepper_process.stdout)
            assert(clingo_process.returncode == 10) # SAT

    for i in noinstances:
        with subprocess.Popen([pepper, i, os.path.join(vmsdir, "guess.lp"), os.path.join(vmsdir, "vertexcover.lp")], stdout=subprocess.PIPE) as pepper_process:
            clingo_process = subprocess.run([clingo, "-q", pimeta, "-"], stdin=pepper_process.stdout)
            assert(clingo_process.returncode == 20) # SAT
