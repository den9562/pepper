#include "gringorun.h"

#include <gringo/scripts.hh>

#include <program_opts/application.h>

GringoRun::GringoRun(Gringo::Output::OutputBase &out, const std::vector<std::string> &files)
    : out(out), scripts(def), pb(scripts, prg, out, defs, false), parser(pb)
{
    for (auto &x : files) {
        parser.pushFile(std::string(x));
    }
}

void GringoRun::ground(Gringo::Control::GroundVec const &parts)
{
    parse();

    if (parsed) {
        prg.rewrite(defs);
        prg.check();

        if (Gringo::message_printer()->hasError()) {
            throw std::runtime_error("grounding stopped because of errors");
        }

        parsed = false;
    }

    grounded = true;

    if (!parts.empty()) {
        Gringo::Ground::Parameters params;

        for (auto &x : parts) {
            params.add(x.first, x.second);
        }

        Gringo::Ground::Program gPrg(prg.toGround(out.domains));
        gPrg.ground(params, scripts, out, false);
    }
}

void GringoRun::load(std::string const &filename)
{
    parser.pushFile(std::string(filename));
    parse();
}

void GringoRun::parse()
{
    if (!parser.empty()) {
        parser.parse();
        defs.init();
        parsed = true;
    }
}

