#ifndef RULECOLLECTOR_H
#define RULECOLLECTOR_H

#include <gringo/output/output.hh>

#include <set>
#include <string>

class RuleCollector : public Gringo::Output::StmHandler
{
public:
    RuleCollector(std::ostream & out) : m_out(out) {}
    virtual void operator()(Gringo::Output::Statement& x);
    virtual void operator()(Gringo::PredicateDomain::element_type &head, Gringo::TruthValue type);
    virtual void finish(Gringo::Output::OutputPredicates& outPreds);
    virtual void atoms(int, std::function<bool(unsigned)> const &, Gringo::ValVec &, Gringo::Output::OutputPredicates const &);
    virtual void simplify(Gringo::Output::AssignmentLookup) { }
    
    std::set<std::string> rules() { return m_rules; }
    
private:
    std::set<std::string> m_rules;
    std::ostream & m_out;
};

#endif // RULECOLLECTOR_H
