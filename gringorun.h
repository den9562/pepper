#ifndef GRINGORUN_H
#define GRINGORUN_H

#include <vector>
#include <string>

#include <clingo/clingocontrol.hh>

#include <gringo/control.hh>
#include <gringo/input/nongroundparser.hh>
#include <gringo/input/program.hh>
#include <gringo/output/output.hh>

class GringoRun
{
public:
    GringoRun(Gringo::Output::OutputBase &out, const std::vector<std::string> &files);
    void ground(Gringo::Control::GroundVec const &parts);
    void load(std::string const &filename);

private:
    void parse();

    bool parsed;
    bool grounded;
    Gringo::Output::OutputBase &out;
    Gringo::Input::Program prg;
    Gringo::Defines defs;
    DefaultGringoModule def;
    Gringo::Scripts scripts;
    Gringo::Input::NongroundProgramBuilder pb;
    Gringo::Input::NonGroundParser parser;
};

#endif // GRINGORUN_H
