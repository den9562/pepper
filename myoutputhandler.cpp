#include "myoutputhandler.h"

#include <cassert>
#include <sstream>
#include <algorithm>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

static const std::string litPred = "lit";
static const std::string atomPred = "atom";
static const std::string inSPred = "inS";
static const std::string ninSPred = "ninS";
static const std::string notokPred = "notok";

void printBody(std::ostream& out, MyOutputHandler::PredLitVec& body)
{
    std::vector<std::string> bguessStrs;

    out << "  :- ";

    for (auto& b : body) {
        std::stringstream tmp;

        b->printPlain(tmp);
        bguessStrs.push_back(tmp.str());
    }

    out << boost::algorithm::join(bguessStrs, ", ");
}

/*
 * This method has the following tasks:
 * 1) Figure out if this statement comes from the guess program
 * 2) If not, normalize the rule
 */
void MyOutputHandler::operator()(Gringo::Output::Statement& x)
{
    std::stringstream repeated;

    x.printPlain(repeated);

    if (m_guessRules.find(repeated.str()) != m_guessRules.end()) {
        // This rule is repeated
        return;
    }

    Gringo::Output::Statement* r = dynamic_cast<Gringo::Output::RuleRef*>(&x);

    if (r) {
        auto rule = dynamic_cast<Gringo::Output::RuleRef*>(&x);
        auto rclone = rule->clone();
        AtomVec head;
        PredLitVec pbody;
        PredLitVec nbody;

        if (rule->head) {
            head.push_back(rule->head->first);
        }

        processBody(rclone->body, head, pbody, nbody);
        processNormalRule(head, pbody, nbody);
    } else {
        // It wasn't a RuleRef, maybe a Disjunction?
        r = dynamic_cast<Gringo::Output::DisjunctionRule*>(&x);

        if (r) {
            auto disjunction = dynamic_cast<Gringo::Output::DisjunctionRule*>(&x);
            AtomVec head;
            PredLitVec pbody;
            PredLitVec nbody;

            for (auto& h : disjunction->repr->elems) {
                auto& l = h.heads.front().front();
                auto* plit = dynamic_cast<Gringo::Output::PredicateLiteral*>(l.get());

                head.push_back(plit->repr->first);
            }

            processBody(disjunction->body, head, pbody, nbody);
            processNormalRule(head, pbody, nbody);
        } else {
            assert(0);
        }
    }

    m_ruleNumber++;
}

void MyOutputHandler::operator()(Gringo::PredicateDomain::element_type& head, Gringo::TruthValue type)
{
    switch (type) {
    case Gringo::TruthValue::False: {
        m_out << "#external " << head.first << ".\n";
        break;
    }
    case Gringo::TruthValue::True:  {
        m_out << "#external " << head.first << "=true.\n";
        break;
    }
    case Gringo::TruthValue::Free:  {
        m_out << "#external " << head.first << "=free.\n";
        break;
    }
    case Gringo::TruthValue::Open:  {
        m_out << "#external " << head.first << "=open.\n";
        break;
    }
    }
}

void MyOutputHandler::finish(Gringo::Output::OutputPredicates &outPreds)
{
    for (auto &x : outPreds) {
        if (std::get<1>(x) != Gringo::Signature("", 0)) {
            m_out << "#show " << (std::get<2>(x) ? "$" : "") << *std::get<1>(x) << ".\n";
        }
        else                                    {
            m_out << "#show.\n";
        }
    }
}

void MyOutputHandler::atoms(int, std::function<bool(unsigned)> const &, Gringo::ValVec &, Gringo::Output::OutputPredicates const &)
{
    std::cerr << "what's this?" << std::endl;
}

MyOutputHandler::PredLitVec MyOutputHandler::bodyGuess(MyOutputHandler::PredLitVec& body)
{
    PredLitVec result;

    // Maybe this check can be simplified by always collecting a and -a?
    for (auto& l : body) {
        std::stringstream tmp;

        // TODO Figure out the way to do this  with FWString
        tmp << l->repr->first;

        if (m_guessAtoms.find(tmp.str()) != m_guessAtoms.end()) {
            result.push_back(l);
        }
    }

    return result;
}

void MyOutputHandler::processBody(Gringo::Output::ULitVec& body, MyOutputHandler::AtomVec& head, MyOutputHandler::PredLitVec& pbody, MyOutputHandler::PredLitVec& nbody)
{
    for (auto& l : body) {
        auto plit = dynamic_cast<Gringo::Output::PredicateLiteral*>(l.get());

        if (plit) {
            assert(plit->repr);

            if (plit->isAtom()) {
                assert(plit->naf == Gringo::NAF::POS);
                pbody.push_back(plit);
            } else {
                assert(plit->naf == Gringo::NAF::NOT);
                nbody.push_back(plit);
            }
        } else {
            auto aggptr = dynamic_cast<Gringo::Output::BodyAggregate*>(l.get());

            assert(aggptr);
            assert(aggptr->fun == Gringo::AggregateFunction::COUNT || aggptr->fun == Gringo::AggregateFunction::SUM);

            std::cerr << "fun " << aggptr->fun << std::endl;

            // I never found out what the Gringo::Value in aggptr->repr->first was
            for (auto& element : aggptr->repr->second.elems) {

                std::cerr << "element.first ";
                std::copy(element.first.begin(), element.first.end(), std::ostream_iterator<Gringo::Value>(std::cerr, " "));
                std::cerr << std::endl;

                for (auto& valVector : element.second) {
                    for (auto& lit : valVector) {
                        lit->printPlain(std::cerr);
                        std::cerr << std::endl;
                    }
                }
            }
        }
    }
}

void MyOutputHandler::processNormalRule(MyOutputHandler::AtomVec& head, MyOutputHandler::PredLitVec& pbody, MyOutputHandler::PredLitVec& nbody)
{
    auto pbguess = bodyGuess(pbody);
    auto nbguess = bodyGuess(nbody);
    PredLitVec bguess(pbguess.size() + nbguess.size());

    std::copy(pbguess.begin(), pbguess.end(), bguess.begin());
    std::copy(nbguess.begin(), nbguess.end(), bguess.begin() + pbguess.size());
    auto rname = Gringo::Value::createId((boost::format("r%1%") % m_ruleNumber).str());

    for (auto& h : head) {
        m_out << Gringo::Value::createFun(litPred, { Gringo::Value::createId("h"), h, rname });

        if (!bguess.empty()) {
            printBody(m_out, bguess);
        }

        m_out << "." << std::endl;

        // The atom/2 part
        m_out << Gringo::Value::createFun(atomPred, { h, h.sign() ? h.flipSign() : h }) << "." << std::endl;
    }

    for (auto& b : pbody) {
        if (std::find(pbguess.begin(), pbguess.end(), b) == pbguess.end()) {
            m_out << Gringo::Value::createFun(litPred, { Gringo::Value::createId("p"), b->repr->first, rname });

            if (!bguess.empty()) {
                printBody(m_out, bguess);
            }

            m_out << "." << std::endl;
        }
    }

    for (auto& b : nbody) {
        if (std::find(nbguess.begin(), nbguess.end(), b) == nbguess.end()) {
            m_out << Gringo::Value::createFun(litPred, { Gringo::Value::createId("n"), b->repr->first, rname });

            if (!bguess.empty()) {
                printBody(m_out, bguess);
            }

            m_out << "." << std::endl;
        }
    }

    if (m_optmod) {
        std::vector<std::string> optlits;

        if (head.size() == 1) {
            m_out << Gringo::Value::createFun(inSPred, { head.front() });

            for (auto& b : pbody) {
                if (std::find(pbguess.begin(), pbguess.end(), b) == pbguess.end()) {
                    std::stringstream tmp;

                    Gringo::Value::createFun(inSPred, { b->repr->first}).print(tmp);
                    optlits.push_back(tmp.str());
                }
            }

            for (auto& b : nbody) {
                if (std::find(nbguess.begin(), nbguess.end(), b) == nbguess.end()) {
                    std::stringstream tmp;

                    Gringo::Value::createFun(ninSPred, { b->repr->first}).print(tmp);
                    optlits.push_back(tmp.str());
                }
            }
        } else {
            m_out << Gringo::Value::createId(notokPred);

            for (auto& h : head) {
                std::stringstream tmp;

                Gringo::Value::createFun(ninSPred, { h }).print(tmp);
                optlits.push_back(tmp.str());
            }

            for (auto& b : pbody) {
                if (std::find(pbguess.begin(), pbguess.end(), b) == pbguess.end()) {
                    std::stringstream tmp;

                    Gringo::Value::createFun(inSPred, { b->repr->first}).print(tmp);
                    optlits.push_back(tmp.str());
                }
            }

            for (auto& b : nbody) {
                if (std::find(nbguess.begin(), nbguess.end(), b) == nbguess.end()) {
                    std::stringstream tmp;

                    Gringo::Value::createFun(ninSPred, { b->repr->first}).print(tmp);
                    optlits.push_back(tmp.str());
                }
            }
        }

        if (!bguess.empty()) {
            printBody(m_out, bguess);

            if (!optlits.empty()) {
                m_out << ", " << boost::algorithm::join(optlits, ", ");
            }
        } else {
            if (!optlits.empty()) {
                m_out << " :- " << boost::algorithm::join(optlits, ", ");
            }
        }

        m_out  << "." << std::endl;
    }
}
