#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <string>
#include <set>
#include <sstream>

#include <gringo/input/nongroundparser.hh>
#include <gringo/input/groundtermparser.hh>
#include <gringo/input/programbuilder.hh>
#include <gringo/input/program.hh>
#include <gringo/ground/program.hh>
#include <gringo/output/output.hh>
#include <gringo/output/statements.hh>
#include <gringo/logger.hh>

#include  "gringorun.h"
#include "rulecollector.h"
#include "myoutputhandler.h"

int main(int argc, char **argv) {
    assert(argc == 4);
    bool optmod = true;
    std::vector<std::string> files { argv[1], argv[2] };
    Gringo::Output::OutputPredicates outPreds;

    Gringo::Output::OutputBase collectorOutput(std::move(outPreds), std::cout);
    GringoRun collectorControl(collectorOutput, files);
    Gringo::Control::GroundVec parts;
    std::set<std::string> guessAtoms;

    // TODO Double check if this leaks memory
    collectorOutput.handler.reset(new RuleCollector(std::cout));

    parts.emplace_back("base", Gringo::FWValVec {});
    collectorControl.ground(parts);

    for (auto &d : collectorOutput.domains)
    {
        if ((*(*d.first).name())[0] == '#') {
            continue;
        }

        for (auto &v : d.second.domain)  {
            std::stringstream tmp;

            tmp << v.first;
            guessAtoms.insert(tmp.str());
        }
    }

    std::ofstream null;
    auto guessRules = dynamic_cast<RuleCollector *>(collectorOutput.handler.get())->rules();
    Gringo::Output::OutputBase trOutput(std::move(outPreds), null);

    std::cerr << guessAtoms.size() << " guess atoms, " << guessRules.size() << " guess rules" << std::endl;

    // TODO Double check if this leaks memory
    trOutput.handler.reset(new MyOutputHandler(std::cout, guessAtoms, guessRules, optmod));
    files.push_back(argv[3]);

    GringoRun trControl(trOutput, files);

    trControl.ground(parts);

    return 0;
}
