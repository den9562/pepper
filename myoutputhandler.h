#ifndef MYOUTPUTHANDLER_H
#define MYOUTPUTHANDLER_H

#include <set>
#include <string>

#include <gringo/output/output.hh>

class MyOutputHandler : public Gringo::Output::StmHandler
{
public:
    //TODO Maybe this should be an AtomState vector?
    typedef std::vector<Gringo::Value> AtomVec;
    typedef std::vector<Gringo::Output::PredicateLiteral*> PredLitVec;
    MyOutputHandler(std::ostream& out, std::set<std::string>& guessAtoms, std::set<std::string>& guessRules, bool optmod) : m_out(out), m_guessAtoms(guessAtoms), m_guessRules(guessRules), m_optmod(optmod), m_ruleNumber(0) { }
    virtual ~MyOutputHandler()                               { }
    virtual void operator()(Gringo::Output::Statement& x);
    virtual void operator()(Gringo::PredicateDomain::element_type& head, Gringo::TruthValue type);
    virtual void finish(Gringo::Output::OutputPredicates& outPreds);
    virtual void atoms(int, std::function<bool(unsigned)> const&, Gringo::ValVec&, Gringo::Output::OutputPredicates const&);
    virtual void simplify(Gringo::Output::AssignmentLookup) { }

private:
    bool m_optmod;
    PredLitVec bodyGuess(PredLitVec& body);
    void processBody(Gringo::Output::ULitVec& body, AtomVec& head, PredLitVec& pbody, PredLitVec& nbody);
    void processNormalRule(AtomVec& head, PredLitVec& pbody, PredLitVec& nbody);

    std::set<std::string>& m_guessAtoms;
    std::set<std::string>& m_guessRules;
    size_t m_ruleNumber;
    std::ostream& m_out;
};

#endif // MYOUTPUTHANDLER_H
