#include "rulecollector.h"

#include <sstream>

void RuleCollector::operator()(Gringo::Output::Statement& x)
{
    std::stringstream tmp;

    x.printPlain(tmp);
    m_rules.insert(tmp.str());

    x.printPlain(m_out);
}

void RuleCollector::operator()(Gringo::PredicateDomain::element_type &head, Gringo::TruthValue type)
{
    switch (type) {
    case Gringo::TruthValue::False: {
        m_out << "#external " << head.first << ".\n";
        break;
    }
    case Gringo::TruthValue::True:  {
        m_out << "#external " << head.first << "=true.\n";
        break;
    }
    case Gringo::TruthValue::Free:  {
        m_out << "#external " << head.first << "=free.\n";
        break;
    }
    case Gringo::TruthValue::Open:  {
        m_out << "#external " << head.first << "=open.\n";
        break;
    }
    }
}

void RuleCollector::finish(Gringo::Output::OutputPredicates& outPreds)
{
    for (auto &x : outPreds) {
        if (std::get<1>(x) != Gringo::Signature("", 0)) {
            std::cout << "#show " << (std::get<2>(x) ? "$" : "") << *std::get<1>(x) << ".\n";
        }
        else                                    {
            std::cout << "#show.\n";
        }
    }
}

void RuleCollector::atoms(int, std::function<bool(unsigned)> const &, Gringo::ValVec &, Gringo::Output::OutputPredicates const &)
{

}
